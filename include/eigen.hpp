#pragma once
#include <memory>
#include <openMVG/numeric/eigen_alias_definition.hpp>

#include "posenet-vr-hub/src/openmvg/eigen.rs.h"

using namespace openMVG;
using namespace std;

// Format
template <typename T>
unique_ptr<string> format_mat(const T &A);
unique_ptr<string> format_mat2x(const Mat2X &A);
unique_ptr<string> format_mat3x(const Mat3X &A);
unique_ptr<string> format_mat34(const Mat34 &A);
unique_ptr<string> format_vec3(const Vec3 &A);
unique_ptr<string> format_vec4(const Vec4 &A);

// To Eigen
unique_ptr<Mat2X> mat2x_from_data(rust::Slice<const double> slice, size_t cols);
unique_ptr<Mat3X> mat3x_from_data(rust::Slice<const double> slice, size_t cols);
unique_ptr<Mat34> mat34_from_data(rust::Slice<const double> slice);
unique_ptr<vector<Mat34>> mat34_vec_from_data(
    rust::Slice<const rust::Slice<const double>> slices);

// To Nalgebra
rust::Slice<const double> mat34_to_slice(const unique_ptr<Mat34> &mat_ptr);
rust::Slice<const double> vec4_to_slice(const unique_ptr<Vec4> &mat_ptr);
